#!/usr/bin/python3
# Het programma pakt een tekstbestand en hypothesebestand, traint zich op het bepalen van entailment en maakt hier vervolgens een voorspelling mee.

from sklearn import preprocessing
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn import metrics
from tabulate import tabulate


# Deze onderstaande functies maken lijsten van de ingelezen data
# Maakt voor de tekst of hypothese een lijst van de inputtekst, elke zin verandert in een lijst me de woorden, deze lijst wordt aan de overkoepelende lijst toegevoegd
def texthypoList(th):
    thlist = []        
    for line in th.readlines():
        thlist.append(line.strip().split())         
    return thlist


# Maakt voor de entailment-, contradictie- of neutralkennis een lijst van het tekstbestand, de waarden worden omgezet in nummers om de classifier te kunnen gebruiken
def entailmentList(e): 
    elist = []    
    for i in e.readlines():
        i = i.split()
        if i[0] == "ENTAILMENT":
            e = "0"
        elif i[0] == "NEUTRAL":
            e = "1"
        else:
            e = "2"
        elist.append((e, i[1]))           
    return elist


# Maakt lijst van de inputtekst met Stanford Dependencies. Elk element in de lijst is een lijst van een zin met de nodige waarden voor dependencies.
def makeTriplet(th):
    sent = []    
    sentlist = []
    for line in th.readlines():
        if line != "\n":
            triple = line.strip()
            for char in triple:
                if char == "(":
                    bracketloc = triple.index(char)
                    postag = triple[:bracketloc]
                    rest = triple[bracketloc+1:-1]
                    break                    
            triplist = [postag]        
            wordposition = rest.split(" ")
            wordposition[0] = wordposition[0][:-1]                       
            for word in wordposition:
                position = ""
                while True:                 
                    if word[-1].isnumeric():
                        position = word[-1] + position
                        word = word[:-1]                       
                    else:
                        word = word[:-1]
                        triplist = triplist + [word, position]
                        break                     
            pt, word1, pos1, word2, pos2 = [str(v) for v in triplist]                                 
            rpos1, rpos2 = int(pos1), int(pos2)            
            if rpos1 > rpos2:    
                rpos1 = rpos1 - rpos2
                rpos2 = 0                
            else:
                rpos2 = rpos2 - rpos1
                rpos1 = 0
            sent.append([pt, word1, pos1, word2, pos2, str(rpos1), str(rpos2)])                                   
        else:                   
            sentlist.append(sent)          
            sent = []            
    return sentlist


# Deze onderstaande functies berekenen de referentiepunten en maken er lijsten van
# Berekent de overlap in Triplets en maakt lijsten van deze overlapscores
def tripletOverlap(t, h):   
    list1 = []
    list2 = []
    list3 = []
    list4 = []
    list5 = []
    list6 = []    
    for sent in range(len(t)): 
        c1, c2, c3, c4, c5, c6 = 0, 0, 0, 0, 0, 0
        s_1 = []
        s_2 = []
        s_3 = []        
        s_4 = []
        s_5 = []
        s_6 = []
        for triplet in range(len(t[sent])):
            trip = t[sent][triplet] 
            s_1.append(trip[:5]) 
            s_2.append([trip[0], trip[1], trip[5], trip[3], trip[6]])
            s_3.append([trip[0], trip[1], trip[3]])           
            s_4.append([trip[1], trip[3]])          
            s_5.append([trip[1], trip[2], trip[3], trip[4]])
            s_6.append([trip[1], trip[5], trip[3], trip[6]])                 
        for triplet in range(len(h[sent])):
            trip = h[sent][triplet]          
            if trip[:5] in s_1:
                c1 += 1
            if [trip[0], trip[1], trip[5], trip[3], trip[6]] in s_2:
                c2 += 1            
            if [trip[0], trip[1], trip[3]] in s_3:
                c3 += 1
            if [trip[1], trip[3]] in s_4:
                c4 += 1    
            if [trip[1], trip[2], trip[3], trip[4]] in s_5:
                c5 += 1
            if [trip[1], trip[5], trip[3], trip[6]] in s_6:
                c6 += 1                
        list1.append(c1)
        list2.append(c2)    
        list3.append(c3)    
        list4.append(c4)    
        list5.append(c5)    
        list6.append(c6)                    
    return [list1, list2, list3, list4, list5, list6] 


# Berekent de overlap in bigrams en maakt een lijst van deze overlapscores
def bigramOverlap(t, h):
    bo = []
    for i in range(len(t)):
        count = 0
        stringt = " ".join(t[i])
        for w in range(len(h[i])-1):
            bigramh = h[i][w]+" "+h[i][w+1]
            if bigramh in stringt:
                count += 1
        bo.append(count)
    return bo
    

# Berekent de overlap in woorden en maakt een lijst van deze overlapscores
def wordOverlap(t, h):
    wo = []
    for i in range(len(t)):
        count = 0
        for woord in h[i]:
            if woord in t[i]:
                count+=1
        wo.append(count)
    return wo
    
    
# Berekent het verschil in zinslengte en maakt een lijst van deze overlapscores
def sentLength(t, h):
    sl = []    
    for i in range(len(t)):
        sl.append(len(t[i])-len(h[i]))        
    return sl           


# Voorspelt entailment met meerderheidsbaseline
def baseline(data):
    train = []
    test = []    
    correct = 0
    total = 0    
    for i in range(len(data)):
        if data[i][1] == "TEST":
            test.append(data[i][0])
        else:
            train.append(data[i][0])
    most_common = max(set(train), key=train.count)    
    prediction = [most_common for i in range(len(test))]   
    for i in range(len(test)):
        if prediction[i] == test[i]:
            correct += 1
        total += 1        
    return round(correct/total*100, 1)
    

# Maakt de train en testset en voorspelt entailment met classifier, returnt het percentage dat goed geraden is
def predictEnt(length, entailment, features):
    X_train = []
    y_train = []
    X_test = []
    y_test = []    
    X_temp = []    
    for i in range(length):
        if entailment[i][1] == "TEST":
            y_test.append(entailment[i][0])
            if isinstance(features[0], list):           
                for f in range(len(features)):
                    X_temp.append(features[f][i])
                X_test.append(X_temp)
                X_temp = []
            else:
                X_test.append([features[i]])        
        else:
            y_train.append(entailment[i][0]) 
            if isinstance(features[0], list):          
                for f in range(len(features)):
                    X_temp.append(features[f][i])
                X_train.append(X_temp)
                X_temp = []
            else:
                X_train.append([features[i]])               
    gnb = GaussianNB()
    gnb.fit(X_train, y_train) 
    y_pred = gnb.predict(X_test)
    return round(metrics.accuracy_score(y_test, y_pred)*100, 1)
 
  
def main():
    st, rt = open("./Textfiles/Features/SICK_text.txt", "r"), open("./Textfiles/Features/RTE_text.txt", "r")
    sh, rh = open("./Textfiles/Features/SICK_hypothesis.txt", "r"), open("./Textfiles/Features/RTE_hypothesis.txt", "r")
    se, re = open("./Textfiles/Features/SICK_entailment.txt", "r"), open("./Textfiles/Features/RTE_entailment.txt", "r")    
    stsd, rtsd = open("./Textfiles/Dependencies/SICK_text.txt", "r"), open("./Textfiles/Dependencies/RTE_text.txt", "r")
    shsd, rhsd = open("./Textfiles/Dependencies/SICK_hypothesis.txt", "r"), open("./Textfiles/Dependencies/RTE_hypothesis.txt", "r")  
    sickrte = [[st, sh, se, "SICK-NL", stsd, shsd], [rt, rh, re, "RTE-3", rtsd, rhsd]] 
      
    for dataset in range(len(sickrte)):
        data = sickrte[dataset]
        
        # Normale tekst, hypothese en entailment komen in een lijst
        text = texthypoList(data[0])
        hypo = texthypoList(data[1])
        entailment = entailmentList(data[2])
        
        # Triplets van tekst en hypothese komen in een lijst
        triptext = makeTriplet(data[4])
        triphypo = makeTriplet(data[5])
        
        # Verschillende overlappen in tripletcombinaties worden berekend
        tripoverlap = tripletOverlap(triptext, triphypo)
        triplist = ["R W PA", "R W PR", "R W", "W", "W PA", "W PR"]
        
        # De verschillende lijsten van de referentiepunten van features worden gemaakt
        sl = sentLength(text, hypo)
        wo = wordOverlap(text, hypo)
        bo = bigramOverlap(text, hypo)
    
        # Meerderheidsbaseline
        bl = baseline(entailment)
                        
        # features alleen
        fbo = predictEnt(len(text), entailment, bo)        
        fwo = predictEnt(len(text), entailment, wo)        
        fsl = predictEnt(len(text), entailment, sl)        
        fcb = predictEnt(len(text), entailment, [bo, wo, sl])               
        
        # De tabel die entailment bepaling laat zien op basis van dependencies, dit wordt ook vergeleken met de baseline  
        table1head = [data[3], "Accuratesse"]
        table1bl   = ["Baseline", bl]
        table1     = [table1head, table1bl]        
        for i in range(len(tripoverlap)):
            acc = predictEnt(len(text), entailment, tripoverlap[i])
            table1.append([triplist[i], acc])
        print(tabulate(table1, headers="firstrow", tablefmt="fancy_grid"))  
        
        # De tabel die entailment bepaling laat zien op basis van features, dit wordt ook vergeleken met de baseline  
        table2head = [data[3], "Accuratesse"]
        table2bl   = ["Baseline", bl]
        table2bo   = ["Bigramoverlap", fbo]
        table2wo   = ["Woordoverlap", fwo]
        table2sl   = ["Zinslengte", fsl]
        table2cb   = ["Combinatie", fcb]
        table2     = [table2head, table2bl, table2bo, table2wo, table2sl, table2cb]        
        print(tabulate(table2, headers="firstrow", tablefmt="fancy_grid")) 
        
        
        # De tabel die dependencies en features combineert om entailment te bepalen    
        featurelijst = ["", "", fbo, fwo, fsl, fcb]        
        table3 = [["", "", "Bigramoverlap", "Woordoverlap", "Zinslengte", "Combinatie"], featurelijst]     
        for i in range(len(tripoverlap)):
            s = triplist[i]
            dep = predictEnt(len(text), entailment, tripoverlap[i])
            depbo = predictEnt(len(text), entailment, [bo, tripoverlap[i]])
            depwo = predictEnt(len(text), entailment, [wo, tripoverlap[i]])
            depsl = predictEnt(len(text), entailment, [sl, tripoverlap[i]])
            depcb = predictEnt(len(text), entailment, [bo, wo, sl, tripoverlap[i]])
            table3.append([s, dep, depbo, depwo, depsl, depcb])
        print(tabulate(table3, headers="firstrow", tablefmt="fancy_grid"))
        
if __name__=="__main__":
    main()
        
