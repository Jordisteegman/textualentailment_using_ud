#!/usr/bin/python3
# Parst de tekst vanuit het SICK-NL corpus en het RTE-3 corpus naar een tekst-, hypothese- en entailmentbestand

import spacy


def parseSICK(corpus, depfiles, feafiles, spacy):

    data = []
    entailmentfiles = [depfiles[2], feafiles[2]]
    
    for line in corpus.readlines()[1:]:
        line = line.split("\t")
        data.append([line[1], line[2], line[3], line[11]])  
          
    for i in range(2): 
        with open(depfiles[i], 'w') as f:           
            for sent in data:
                parse = spacy(sent[i])               
                for token in parse:
                    if token.head.i == token.i:
                        tokenhead = "ROOT"
                        tokenheadi = 0
                    else:
                        tokenhead = token.head
                        tokenheadi = token.head.i+1
                    f.write("{}({}-{}, {}-{})\n".format(str(token.dep_), str(tokenhead), str(tokenheadi), str(token.text), str(token.i+1)))          
                f.write("\n")
                
        with open(feafiles[i], 'w') as f:            
            for sent in data:
                f.write(sent[i])
                f.write("\n")
                
        with open(entailmentfiles[i], 'w') as f:
            for i in range(len(data)):
                f.write(data[i][2]+"\t"+data[i][3])
 
            
def parseRTE(corpus, depfiles, feafiles, spacy):

    data = []
    count = 0
    text = ""
    hypothesis = ""
    entailmentfiles = [depfiles[2], feafiles[2]]
    
    for line in corpus.readlines():
        if count == 4:
            data.append([text, hypothesis, lineinfo[2], lineinfo[1]])
            count = 0
            
        if count == 0 or count == 2:       
            lineinfo = line.strip().split()

        elif count != 4:
            if count == 1:
                if lineinfo[3] == "h":
                    hypothesis = line.strip()
                elif lineinfo[3] == "t":
                    text = line.strip()
            elif count == 3:
                if lineinfo[3] == "h":
                    hypothesis = line.strip()
                elif lineinfo[3] == "t":
                    text = line.strip()                    
        count += 1
        
    for i in range(2): 
        with open(depfiles[i], 'w') as f:            
            for sent in data:
                parse = spacy(sent[i])               
                for token in parse:
                    if token.head.i == token.i:
                        tokenhead = "ROOT"
                        tokenheadi = 0
                    else:
                        tokenhead = token.head
                        tokenheadi = token.head.i+1
                    f.write("{}({}-{}, {}-{})\n".format(str(token.dep_), str(tokenhead), str(tokenheadi), str(token.text), str(token.i+1))) # str(token.tag_)            
                f.write("\n")
                
        with open(feafiles[i], 'w') as f:           
            for sent in data:
                f.write(sent[i])
                f.write("\n")
                
        with open(entailmentfiles[i], 'w') as f:
            for i in range(len(data)):
                if data[i][2] == "YES":
                    e = "ENTAILMENT"
                else:
                    e = "NEUTRAL"
            
                if data[i][3] == "dev":
                    t = "TRAIN"
                else:
                    t = "TEST"
            
                f.write(e+"\t"+t+"\n")
              
    
def main():

    nl_nlp = spacy.load('nl_core_news_sm')
    
    sicknl = open("./Textfiles/Corpora/SICK_NL.txt", "r")
    sickdepfiles = ["./Textfiles/Dependencies/SICK_text.txt", "./Textfiles/Dependencies/SICK_hypothesis.txt", "./Textfiles/Dependencies/SICK_entailment.txt"]
    sickfeafiles = ["./Textfiles/Features/SICK_text.txt", "./Textfiles/Features/SICK_hypothesis.txt", "./Textfiles/Features/SICK_entailment.txt"]
    parseSICK(sicknl, sickdepfiles, sickfeafiles, nl_nlp)
    
    rte = open("./Textfiles/Corpora/RTE3.txt", "r")    
    rtedepfiles = ["./Textfiles/Dependencies/RTE_text.txt", "./Textfiles/Dependencies/RTE_hypothesis.txt", "./Textfiles/Dependencies/RTE_entailment.txt"]
    rtefeafiles = ["./Textfiles/Features/RTE_text.txt", "./Textfiles/Features/RTE_hypothesis.txt", "./Textfiles/Features/RTE_entailment.txt"]
    parseRTE(rte, rtedepfiles, rtefeafiles, nl_nlp)

if __name__=="__main__":
    main()
